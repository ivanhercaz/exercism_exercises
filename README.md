This repository is just a compilation of my exercises done on different tracks
of [Exercism](https://exercism.org). My aim is to keep it updated according my
progress on the tracks and provide on each exercise a link to the exercise, the
track and my solution.

Haven't tried Exercism yet?! Although I use less than what I would like, you can
check [my profile](https://exercism.org/profiles/ivanhercaz) and the [available
tracks](https://exercism.org/tracks). Exercism is really nice to learn concepts
and promote learning dinamically aplying the theory to practical exercises.

Before to check the files of the exercises, my advice is that if you are doing one
that it is inside this repository, first try by yourself, research about the exercise
and try again. Use this solutions as reference in case you need it!

Many user's solutions are similar or even the same, the idea is not be the most
original solution (unless you are trying to "refactor" yourself!), the idea
behind all of these exercises is to learn! I have learn a lot reading other user's 
solutions.

 ## Tracks

Each link points to a subdirectory of this repository in which there is a
specific README with the exercises I have done, and then, each exercise has its
own README.

I am on the next tracks:
 - [Elixir](/ivanhercaz/exercism_exercises/src/branch/main/elixir).
 - [Prolog](/ivanhercaz/exercism_exercises/src/branch/main/prolog).

 ## License

This repository is released into the public domain. You are free to use it as
you please ([LICENSE](/ivanhercaz/exercism_exercises/src/branch/main/LICENSE)). But it is important to note that this license refers to the solution
I provided. Everything else may be under another license, licensed by Exercism
or one of its contributors.

