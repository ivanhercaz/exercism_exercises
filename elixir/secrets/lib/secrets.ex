defmodule Secrets do
  # This functions just returns anonymous functions so I didn't see it necessary
  # to add them to variables due to the use they will have.

  def secret_add(secret) do
    fn x ->
      x + secret
    end
  end

  def secret_subtract(secret) do
    fn x ->
      x - secret
    end
  end

  def secret_multiply(secret) do
    fn x ->
      x * secret
    end
  end

  def secret_divide(secret) do
    # In this case I use `div/2` function to return a integer, because
    # the operator `/` returns a float number and the tests asserts
    # the results equals to integers.
    fn x ->
      div(x, secret)
    end
  end

  def secret_and(secret) do
    fn x ->
      Bitwise.&&&(x, secret)
    end
  end

  def secret_xor(secret) do
    fn x ->
      Bitwise.^^^(x, secret)
    end
  end

  def secret_combine(secret_function1, secret_function2) do
    fn x ->
      x
      |> secret_function1.()
      |> secret_function2.()
    end
  end
end
